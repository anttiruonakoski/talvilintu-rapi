<?php

/**
 * Lintutieteellisen Yhdistyksen alueella laskentakaudella lasketut talvilintulaskennat -taulukko
 * Hakee tiedot laji.fi rajapinnasta
 * 
 * @author Antti Ruonakoski <aruonakoski@gmail.com>
 * @copyright Antti Ruonakoski 2019
 * 
 * @var string birdAssociationAreaId Lintutieteellisen yhdistyksen id 
 * @var boolean calculateYear saako laskentakausi vakion arvon vai määräytyykö dynaamisesti 
 * 
 * @param string|int $archive_year syyslaskennan vuosi 
 * 
 * @return string HTML div-elementti, joka sisältää 3 taulukkona laskentakauden laskennat 
 */

chdir(dirname(__FILE__)); //to correct paths under WordPress 
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use Twig\TwigFilter;

require_once './vendor/autoload.php';
$loader = new FilesystemLoader('./subtemplates');
$twig = new Environment($loader, array('debug' => true)
);
// $twig->addExtension(new Twig_Extension_Debug());

$twig->addFilter( new TwigFilter('cast_to_array', function ($stdClassObject) {
    $response = array();
    foreach ($stdClassObject as $key => $value) {
        $response[] = array($key, $value);
    }
    return $response;
}));

include('./includes/httpful.phar');
define('birdAssociationAreaId', 'ML.1112');
define('calculateYear', TRUE);

// 
$archive_year = $archive_year ?? NULL;
// var_dump($archive_year);

// $calculateYear = TRUE;
$defaultYearMonth = '2019-10/2020-03';

if ( calculateYear ) {
    $octoberEnd = DateTime::createFromFormat('m-d', '10-30'); // initialize new year on October 30th.  
    // haetaan aiemmat laskennat, joiden syyslaskennan vuosi annettu
    if ( $archive_year ) {
        $now = DateTime::createFromFormat('m-d-Y', '12-01-' . $archive_year);
    } // käytetään kuluvaa vuotta
    else {
        $now = new DateTime();
    }
    if ( $archive_year || ( $now > $octoberEnd ) ) {
        $yearMonth = $now->format('Y') . '-10/' . ((int)($now->format('Y'))+1) . '-03';  
        } else {
        $yearMonth = ((int)($now->format('Y'))-1) . '-10/' . $now->format('Y') . '-03';
        };
} else {
    $yearMonth = $defaultYearMonth;
};

/* $uri = 'https://laji.fi/api/warehouse/query/aggregate?collectionId=HR.39&birdAssociationAreaId=' . birdAssociationAreaId . '&yearMonth=' . $yearMonth . '&selected=document.documentId,document.namedPlace.name,document.namedPlace.municipalityDisplayName,gathering.eventDate.begin,gathering.team&aggregateBy=document.documentId,document.namedPlace.name,document.namedPlace.municipalityDisplayName,document.namedPlace.birdAssociationAreaDisplayName,gathering.eventDate.begin,gathering.team&orderBy=document.namedPlace.birdAssociationAreaDisplayName,gathering.eventDate.begin%20ASC&pageSize=10000&page=1&onlyCount=false'; */

// statistic endpointista saa laji- ja yksilömäärät mukaanlukien sensitiiviset lajit, mutta siitä ei saa gathering.team dataa. Joten tarvitaan 2 requestia.

$uri_statistics = 'https://laji.fi/api/warehouse/query/statistics?collectionId=HR.39&birdAssociationAreaId=' . birdAssociationAreaId . '&yearMonth=' . $yearMonth . '&aggregateBy=document.documentId,document.namedPlace.name,document.namedPlace.municipalityDisplayName,document.namedPlace.birdAssociationAreaDisplayName&selected=document.documentId&orderBy=oldestRecord%20ASC,document.namedPlace.name%20ASC&page=1&pageSize=10000&onlyCount=false';

$uri_aggregate = 'https://laji.fi/api/warehouse/query/aggregate?collectionId=HR.39&birdAssociationAreaId=' . birdAssociationAreaId . '&yearMonth=' . $yearMonth . '&selected=document.documentId,gathering.eventDate.begin,gathering.team&aggregateBy=document.documentId,document.namedPlace.birdAssociationAreaDisplayName,gathering.team&orderBy=document.namedPlace.birdAssociationAreaDisplayName%20ASC&pageSize=10000&page=1&onlyCount=true';

// dump($uri_statistics, $uri_aggregate);

try {
    $statistics_response = \Httpful\Request::get($uri_statistics)->timeoutIn(10)->autoParse(true)->send();
    $aggregate_response = \Httpful\Request::get($uri_aggregate)->timeoutIn(10)->autoParse(true)->send();
}
catch(Exception $e) {
    echo('Laskettujen reittien näyttäminen epäonnistui. Kokeile myöhemmin uudelleen.');
    exit;
};

$data = $statistics_response->body->results;
$aggregate_temp_data = $aggregate_response->body->results;

// muutetaan assoc taulukoksi aggregate data, jossa on gathering.team eli havainnoijien nimet
$aggregate_data = [];
foreach ( $aggregate_temp_data as $v) {
    $document_id = explode('/', $v->aggregateBy->{'document.documentId'});
    $aggregate_data[array_pop($document_id)] = $v;  
}

// dd($aggregate_data);
// dd($data);

$data_with_gathering_team = array_map(function ($item) use ($aggregate_data) {
    $document_id = explode('/', $item->aggregateBy->{'document.documentId'});
    // dump($document_id);
    $item->team = $aggregate_data[array_pop($document_id)]->aggregateBy->{'gathering.team'};
    return $item;
},
$data 
);
$data = $data_with_gathering_team;

function getPeriodData($period, $data) {
    $filteredData = array_filter($data, function ($item) use ($period) {
        $alku = $item->oldestRecord;
        if (in_array(explode('-', $alku)[1], $period) === true) { //räjäytä kk stringista eli ISO-8061 daten toinen osa
            // echo('säilytetään '. $item->aggregateBy->{'document.namedPlace.name'} . PHP_EOL);
            return true;
        };
    });
    return $filteredData;
};

function countAggregates($data) {
    
    $agg = array();
    $agg['censusCount'] = count((array)$data);
    $agg['avgSpecies'] = array_sum(array_column($data, 'count')) / $agg['censusCount'];
    $agg['avgIndividuals'] = array_sum(array_column($data, 'individualCountSum')) / $agg['censusCount']; 
    return $agg;
};

$laskennat = array('syys' => array('Syyslaskenta', array (10, 11)),
    'talvi' => array('Talvilaskenta', array (12, 1)),
    'kevat' => array('Kevätlaskenta', array(2, 3))
);
// no passion for php array iterations any more. uhoh needs to be reused for aggregates. uh must loop

echo '<div id="lasketut-reitit">';
foreach ($laskennat as $key => $value) {
    $periodData = getPeriodData($value[1], $data);
    // print_r($periodData);
    $aggregateData = ($periodData) ? countAggregates($periodData) : '';
    echo $twig->render('table.html', array('laskenta' => $value[0], 'data' => $periodData, 'aggdata' => $aggregateData));
};
echo '</div>';
//echo $twig->render('table.html', array('laskenta' => $laskennat['talvi'], 'data' => getPeriodData($talvi, $data)));
//echo $twig->render('table.html', array('laskenta' => $laskennat['kevat'], 'data' => getPeriodData($kevat, $data)));
?>
